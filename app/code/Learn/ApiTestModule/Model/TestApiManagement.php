<?php

namespace Learn\ApiTestModule\Model;

class TestApiManagement implements \Learn\ApiTestModule\Api\TestApiManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function getRandomNumber()
    {
        return rand(1,99);
    }
}
