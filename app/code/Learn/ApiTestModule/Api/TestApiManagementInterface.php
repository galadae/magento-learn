<?php

namespace Learn\ApiTestModule\Api;

interface TestApiManagementInterface
{

    /**
     * @return string
     */
    public function getRandomNumber();
}
