<?php

namespace Learn\ConsoleCommandModule\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LearnAction extends Command
{

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        var_dump(rand(1,99));
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("learn:command");
        $this->setDescription("SDesc");
        parent::configure();
    }
}
