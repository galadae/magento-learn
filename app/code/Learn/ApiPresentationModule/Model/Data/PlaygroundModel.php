<?php


namespace Learn\ApiPresentationModule\Model\Data;

use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface;

class PlaygroundModel extends \Magento\Framework\Api\AbstractExtensibleObject implements PlaygroundModelInterface
{

    /**
     * Get playgroundmodel_id
     * @return string|null
     */
    public function getPlaygroundmodelId()
    {
        return $this->_get(self::PLAYGROUNDMODEL_ID);
    }

    /**
     * Set playgroundmodel_id
     * @param string $playgroundmodelId
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function setPlaygroundmodelId($playgroundmodelId)
    {
        return $this->setData(self::PLAYGROUNDMODEL_ID, $playgroundmodelId);
    }

    /**
     * Get Name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
