<?php


namespace Learn\ApiPresentationModule\Model;

use Learn\ApiPresentationModule\Api\PlaygroundModelRepositoryInterface;
use Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel\Collection;
use Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface;
use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterfaceFactory;

class ApiExampleManagement implements \Learn\ApiPresentationModule\Api\ApiExampleManagementInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PlaygroundModelInterfaceFactory
     */
    private $playgroundModelFactory;

    /**
     * @var PlaygroundModelRepositoryInterface $repository
     */
    protected $repository;

    /**
     * @var Collection $pgmc
     */
    protected $pgmc;

    /**
     * @var \Magento\Framework\App\ObjectManager $om
     */
    protected $om;

    public function __construct(
        CollectionFactory $collectionFactory,
        PlaygroundModelInterfaceFactory $playgroundModelFactory,
        PlaygroundModelRepositoryInterface $repository
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->playgroundModelFactory = $playgroundModelFactory;
        $this->repository = $repository;
    }

    public function getApiExample()
    {
        return $this->pgmc->getItems();
    }

    public function getApiExampleById($id)
    {
        return $this->repository->getById($id);
    }

    public function postApiExample($name)
    {
        /** @var PlaygroundModelInterface $newItem */
        $newItem = $this->playgroundModelFactory->create();
        $newItem->setName($name);
        return $this->repository->save($newItem);
    }

    public function putApiExample($id, $name)
    {
        $item = $this->repository->getById($id);
        $item->setName($name);
        return $this->repository->save($item);
    }

    public function deleteApiExample($id)
    {
        return $this->repository->delete($this->repository->getById($id));
    }

}