<?php


namespace Learn\ApiPresentationModule\Model;

use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface;
use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PlaygroundModel extends \Magento\Framework\Model\AbstractModel
{

    protected $playgroundmodelDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'learn_apipresentationmodule_playgroundmodel';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PlaygroundModelInterfaceFactory $playgroundmodelDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel $resource
     * @param \Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PlaygroundModelInterfaceFactory $playgroundmodelDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel $resource,
        \Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel\Collection $resourceCollection,
        array $data = []
    ) {
        $this->playgroundmodelDataFactory = $playgroundmodelDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve playgroundmodel model with playgroundmodel data
     * @return PlaygroundModelInterface
     */
    public function getDataModel()
    {
        $playgroundmodelData = $this->getData();
        
        $playgroundmodelDataObject = $this->playgroundmodelDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $playgroundmodelDataObject,
            $playgroundmodelData,
            PlaygroundModelInterface::class
        );
        
        return $playgroundmodelDataObject;
    }
}
