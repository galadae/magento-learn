<?php


namespace Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Learn\ApiPresentationModule\Model\PlaygroundModel::class,
            \Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel::class
        );
    }
}
