<?php


namespace Learn\ApiPresentationModule\Model\ResourceModel;

class PlaygroundModel extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('learn_apipresentationmodule_playgroundmodel', 'playgroundmodel_id');
    }
}
