<?php


namespace Learn\ApiPresentationModule\Model;

use Learn\ApiPresentationModule\Api\PlaygroundModelRepositoryInterface;
use Learn\ApiPresentationModule\Api\Data\PlaygroundModelSearchResultsInterfaceFactory;
use Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel as ResourcePlaygroundModel;
use Learn\ApiPresentationModule\Model\ResourceModel\PlaygroundModel\CollectionFactory as PlaygroundModelCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class PlaygroundModelRepository implements PlaygroundModelRepositoryInterface
{

    protected $resource;

    protected $playgroundModelFactory;

    protected $playgroundModelCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataPlaygroundModelFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourcePlaygroundModel $resource
     * @param PlaygroundModelFactory $playgroundModelFactory
     * @param PlaygroundModelInterfaceFactory $dataPlaygroundModelFactory
     * @param PlaygroundModelCollectionFactory $playgroundModelCollectionFactory
     * @param PlaygroundModelSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePlaygroundModel $resource,
        PlaygroundModelFactory $playgroundModelFactory,
        PlaygroundModelInterfaceFactory $dataPlaygroundModelFactory,
        PlaygroundModelCollectionFactory $playgroundModelCollectionFactory,
        PlaygroundModelSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->playgroundModelFactory = $playgroundModelFactory;
        $this->playgroundModelCollectionFactory = $playgroundModelCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPlaygroundModelFactory = $dataPlaygroundModelFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
    ) {
        /* if (empty($playgroundModel->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $playgroundModel->setStoreId($storeId);
        } */
        
        $playgroundModelData = $this->extensibleDataObjectConverter->toNestedArray(
            $playgroundModel,
            [],
            \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface::class
        );
        
        $playgroundModelModel = $this->playgroundModelFactory->create()->setData($playgroundModelData);
        
        try {
            $this->resource->save($playgroundModelModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the playgroundModel: %1',
                $exception->getMessage()
            ));
        }
        return $playgroundModelModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($playgroundModelId)
    {
        $playgroundModel = $this->playgroundModelFactory->create();
        $this->resource->load($playgroundModel, $playgroundModelId);
        if (!$playgroundModel->getId()) {
            throw new NoSuchEntityException(__('PlaygroundModel with id "%1" does not exist.', $playgroundModelId));
        }
        return $playgroundModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->playgroundModelCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
    ) {
        try {
            $playgroundModelModel = $this->playgroundModelFactory->create();
            $this->resource->load($playgroundModelModel, $playgroundModel->getPlaygroundmodelId());
            $this->resource->delete($playgroundModelModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PlaygroundModel: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($playgroundModelId)
    {
        return $this->delete($this->getById($playgroundModelId));
    }
}
