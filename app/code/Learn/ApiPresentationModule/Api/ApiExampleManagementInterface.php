<?php

namespace Learn\ApiPresentationModule\Api;

interface ApiExampleManagementInterface
{

    /**
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface[]
     */
    public function getApiExample();

    /**
     * @param int $id
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function getApiExampleById($id);

    /**
     * @param string $name
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function postApiExample($name);

    /**
     * @param int $id
     * @param string $name
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function putApiExample($id, $name);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteApiExample($id);
}