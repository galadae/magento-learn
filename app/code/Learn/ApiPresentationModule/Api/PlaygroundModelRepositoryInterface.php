<?php


namespace Learn\ApiPresentationModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PlaygroundModelRepositoryInterface
{

    /**
     * Save PlaygroundModel
     * @param \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
    );

    /**
     * Retrieve PlaygroundModel
     * @param string $playgroundmodelId
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($playgroundmodelId);

    /**
     * Retrieve PlaygroundModel matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PlaygroundModel
     * @param \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface $playgroundModel
    );

    /**
     * Delete PlaygroundModel by ID
     * @param string $playgroundmodelId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($playgroundmodelId);
}
