<?php


namespace Learn\ApiPresentationModule\Api\Data;

interface PlaygroundModelSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PlaygroundModel list.
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface[]
     */
    public function getItems();

    /**
     * Set Name list.
     * @param \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
