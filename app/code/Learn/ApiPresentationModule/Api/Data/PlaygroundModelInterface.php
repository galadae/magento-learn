<?php


namespace Learn\ApiPresentationModule\Api\Data;

//Ettől az extendálás miatt bővíthető extension attributes -sal ez az api objektum
interface PlaygroundModelInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const PLAYGROUNDMODEL_ID = 'playgroundmodel_id';

    /**
     * Get playgroundmodel_id
     * @return int|null
     */
    public function getPlaygroundmodelId();

    /**
     * Set playgroundmodel_id
     * @param int $playgroundmodelId
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function setPlaygroundmodelId($playgroundmodelId);

    /**
     * Get Name
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ApiPresentationModule\Api\Data\PlaygroundModelExtensionInterface $extensionAttributes
    );
}
