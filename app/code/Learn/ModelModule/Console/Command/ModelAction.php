<?php

namespace Learn\ModelModule\Console\Command;

use Learn\ModelModule\Model\Author;
use Learn\ModelModule\Model\AuthorRepository;
use Learn\ModelModule\Model\Book;
use Learn\ModelModule\Model\BookRepository;
use Magento\Framework\ObjectManager\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ModelAction extends Command
{

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var AuthorRepository $AR */
        $AR=$objectManager->get(AuthorRepository::class);
        /** @var BookRepository $BR */
        $BR=$objectManager->get(BookRepository::class);

        /** @var Author $author */
        $author = $objectManager->create(Author::class);
        /** @var Book $book */
        $book = $objectManager->create(Book::class);

        try {
            $author->setName('Author');

            $author->save();
            //$AR->save($author->getDataModel());

            $book->setName('Book');
            $book->setAuthor($author->getId());

            $book->save();
            //$BR->save($book->getDataModel());

            $output->writeln("author id: " . $author->getId());
            $output->writeln("author name: " . $author->getName());
            $output->writeln("book id: " . $book->getId());
            $output->writeln("book name: " . $book->getName());
            $output->writeln("book author: " . $book->getAuthor());
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("learn:model");
        $this->setDescription("SDesc");
        parent::configure();
    }
}
