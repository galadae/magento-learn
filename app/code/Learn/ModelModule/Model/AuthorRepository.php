<?php


namespace Learn\ModelModule\Model;

use Learn\ModelModule\Api\AuthorRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Learn\ModelModule\Model\ResourceModel\Author as ResourceAuthor;
use Learn\ModelModule\Model\ResourceModel\Author\CollectionFactory as AuthorCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class AuthorRepository implements AuthorRepositoryInterface
{

    protected $resource;

    protected $authorFactory;

    protected $authorCollectionFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceAuthor $resource
     * @param AuthorFactory $authorFactory
     * @param AuthorCollectionFactory $authorCollectionFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceAuthor $resource,
        AuthorFactory $authorFactory,
        AuthorCollectionFactory $authorCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    )
    {
        $this->resource = $resource;
        $this->authorFactory = $authorFactory;
        $this->authorCollectionFactory = $authorCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Learn\ModelModule\Api\Data\AuthorInterface $author
    )
    {
        /* if (empty($author->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $author->setStoreId($storeId);
        } */

        $authorData = $this->extensibleDataObjectConverter->toNestedArray(
            $author,
            [],
            \Learn\ModelModule\Api\Data\AuthorInterface::class
        );

        $authorModel = $this->authorFactory->create()->setData($authorData);

        try {
            $this->resource->save($authorModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the author: %1',
                $exception->getMessage()
            ));
        }
        return $authorModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($authorId)
    {
        $author = $this->authorFactory->create();
        $this->resource->load($author, $authorId);
        if (!$author->getId()) {
            throw new NoSuchEntityException(__('Author with id "%1" does not exist.', $authorId));
        }
        return $author->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    )
    {
        $collection = $this->authorCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Learn\ModelModule\Api\Data\AuthorInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Learn\ModelModule\Api\Data\AuthorInterface $author
    )
    {
        try {
            $authorModel = $this->authorFactory->create();
            $this->resource->load($authorModel, $author->getAuthorId());
            $this->resource->delete($authorModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Author: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($authorId)
    {
        return $this->delete($this->getById($authorId));
    }
}
