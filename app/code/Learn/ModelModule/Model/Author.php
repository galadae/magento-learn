<?php


namespace Learn\ModelModule\Model;

use Learn\ModelModule\Api\Data\AuthorInterface;
use Learn\ModelModule\Api\Data\AuthorInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * Class Author
 * @package Learn\ModelModule\Model
 * @method setName($name)
 * @method getName()
 */
class Author extends \Magento\Framework\Model\AbstractModel
{

    protected $authorDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'learn_modelmodule_author';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param AuthorInterfaceFactory $authorDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Learn\ModelModule\Model\ResourceModel\Author $resource
     * @param \Learn\ModelModule\Model\ResourceModel\Author\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        AuthorInterfaceFactory $authorDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Learn\ModelModule\Model\ResourceModel\Author $resource,
        \Learn\ModelModule\Model\ResourceModel\Author\Collection $resourceCollection,
        array $data = []
    ) {
        $this->authorDataFactory = $authorDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve author model with author data
     * @return AuthorInterface
     */
    public function getDataModel()
    {
        $authorData = $this->getData();

        $authorDataObject = $this->authorDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $authorDataObject,
            $authorData,
            AuthorInterface::class
        );

        return $authorDataObject;
    }
}
