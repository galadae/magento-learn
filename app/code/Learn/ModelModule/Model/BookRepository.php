<?php


namespace Learn\ModelModule\Model;

use Learn\ModelModule\Api\BookRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Learn\ModelModule\Model\ResourceModel\Book as ResourceBook;
use Learn\ModelModule\Model\ResourceModel\Book\CollectionFactory as BookCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class BookRepository implements BookRepositoryInterface
{

    protected $resource;

    protected $bookFactory;

    protected $bookCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataBookFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceBook $resource
     * @param BookFactory $bookFactory
     * @param BookCollectionFactory $bookCollectionFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceBook $resource,
        BookFactory $bookFactory,
        BookCollectionFactory $bookCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    )
    {
        $this->resource = $resource;
        $this->bookFactory = $bookFactory;
        $this->bookCollectionFactory = $bookCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Learn\ModelModule\Api\Data\BookInterface $book
    )
    {
        /* if (empty($book->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $book->setStoreId($storeId);
        } */

        $bookData = $this->extensibleDataObjectConverter->toNestedArray(
            $book,
            [],
            \Learn\ModelModule\Api\Data\BookInterface::class
        );

        $bookModel = $this->bookFactory->create()->setData($bookData);

        try {
            $this->resource->save($bookModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the book: %1',
                $exception->getMessage()
            ));
        }
        return $bookModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($bookId)
    {
        $book = $this->bookFactory->create();
        $this->resource->load($book, $bookId);
        if (!$book->getId()) {
            throw new NoSuchEntityException(__('Book with id "%1" does not exist.', $bookId));
        }
        return $book->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    )
    {
        $collection = $this->bookCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Learn\ModelModule\Api\Data\BookInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Learn\ModelModule\Api\Data\BookInterface $book
    )
    {
        try {
            $bookModel = $this->bookFactory->create();
            $this->resource->load($bookModel, $book->getBookId());
            $this->resource->delete($bookModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Book: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($bookId)
    {
        return $this->delete($this->getById($bookId));
    }
}
