<?php


namespace Learn\ModelModule\Model\ResourceModel;

class Author extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('itegration_author', 'id');
    }
}
