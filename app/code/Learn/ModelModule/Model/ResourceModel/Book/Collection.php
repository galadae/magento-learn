<?php


namespace Learn\ModelModule\Model\ResourceModel\Book;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Learn\ModelModule\Model\Book::class,
            \Learn\ModelModule\Model\ResourceModel\Book::class
        );
    }
}
