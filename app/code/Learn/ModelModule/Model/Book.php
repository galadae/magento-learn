<?php


namespace Learn\ModelModule\Model;

use Learn\ModelModule\Api\Data\BookInterface;
use Learn\ModelModule\Api\Data\BookInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * Class Author
 * @package Learn\ModelModule\Model
 * @method setName($name)
 * @method getName()
 * @method setAuthor($author)
 * @method getAuthor()
 */
class Book extends \Magento\Framework\Model\AbstractModel
{

    protected $bookDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'learn_modelmodule_book';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BookInterfaceFactory $bookDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Learn\ModelModule\Model\ResourceModel\Book $resource
     * @param \Learn\ModelModule\Model\ResourceModel\Book\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BookInterfaceFactory $bookDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Learn\ModelModule\Model\ResourceModel\Book $resource,
        \Learn\ModelModule\Model\ResourceModel\Book\Collection $resourceCollection,
        array $data = []
    )
    {
        $this->bookDataFactory = $bookDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve book model with book data
     * @return BookInterface
     */
    public function getDataModel()
    {
        $bookData = $this->getData();

        $bookDataObject = $this->bookDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $bookDataObject,
            $bookData,
            BookInterface::class
        );

        return $bookDataObject;
    }
}
