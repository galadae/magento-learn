<?php


namespace Learn\ModelModule\Model\Data;

use Learn\ModelModule\Api\Data\AuthorInterface;
use Learn\ModelModule\Api\Data\BookInterface;
use Learn\ModelModule\Model\AuthorRepository;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\ObjectManager;

class Book extends \Magento\Framework\Api\AbstractExtensibleObject implements BookInterface
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $attributeValueFactory,
        AuthorRepository $authorRepository,
        array $data = []
    )
    {
        parent::__construct($extensionFactory, $attributeValueFactory, $data);
        $this->authorRepository = $authorRepository;
    }

    /**
     * Get book_id
     * @return string|null
     */
    public function getBookId()
    {
        return $this->_get(self::AUTHOR_ID);
    }

    /**
     * Get Name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ModelModule\Api\Data\BookInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Author
     * @return AuthorInterface|null
     */
    public function getAuthor()
    {
        return $this->_get(self::AUTHOR);

        /*try {
            $author = $this->authorRepository->getById($this->_get(self::AUTHOR));
        } catch (\Exception $exception) {
            $author = null;
        }*/
        //return $author;
    }

    /**
     * Set Name
     * @param string $authorId
     * @return \Learn\ModelModule\Api\Data\BookInterface
     */
    public function setAuthor($authorId)
    {
        return $this->setData(self::AUTHOR, $authorId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ModelModule\Api\Data\BookExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Learn\ModelModule\Api\Data\BookExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ModelModule\Api\Data\BookExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
