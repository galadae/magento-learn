<?php


namespace Learn\ModelModule\Model\Data;

use Learn\ModelModule\Api\Data\AuthorInterface;

class Author extends \Magento\Framework\Api\AbstractExtensibleObject implements AuthorInterface
{

    /**
     * Get author_id
     * @return string|null
     */
    public function getAuthorId()
    {
        return $this->_get(self::AUTHOR_ID);
    }

    /**
     * Set author_id
     * @param string $authorId
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     */
    public function setAuthorId($authorId)
    {
        return $this->setData(self::AUTHOR_ID, $authorId);
    }

    /**
     * Get Name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ModelModule\Api\Data\AuthorExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Learn\ModelModule\Api\Data\AuthorExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ModelModule\Api\Data\AuthorExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
