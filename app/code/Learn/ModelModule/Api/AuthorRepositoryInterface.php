<?php


namespace Learn\ModelModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface AuthorRepositoryInterface
{

    /**
     * Save Author
     * @param \Learn\ModelModule\Api\Data\AuthorInterface $Author
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Learn\ModelModule\Api\Data\AuthorInterface $Author
    );

    /**
     * Retrieve Author
     * @param string $authorId
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($authorId);

    /**
     * Retrieve Author matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Learn\ModelModule\Api\Data\AuthorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Author
     * @param \Learn\ModelModule\Api\Data\AuthorInterface $Author
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Learn\ModelModule\Api\Data\AuthorInterface $Author
    );

    /**
     * Delete Author by ID
     * @param string $authorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($authorId);
}
