<?php


namespace Learn\ModelModule\Api\Data;

interface BookSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Book list.
     * @return \Lear\ModelModule\Api\Data\BookInterface[]
     */
    public function getItems();

    /**
     * Set Name list.
     * @param \Lear\ModelModule\Api\Data\BookInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
