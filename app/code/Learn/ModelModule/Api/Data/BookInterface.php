<?php


namespace Learn\ModelModule\Api\Data;

interface BookInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const AUTHOR_ID = 'id';
    const NAME = 'name';
    const AUTHOR = 'author';

    /**
     * Get book_id
     * @return string|null
     */
    public function getBookId();

    /**
     * Get Name
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ModelModule\Api\Data\BookInterface
     */
    public function setName($name);

    /**
     * Get Author
     * @return \Learn\ModelModule\Api\Data\AuthorInterface|null
     */
    public function getAuthor();

    /**
     * Set Author
     * @param \Learn\ModelModule\Api\Data\AuthorInterface $author
     * @return \Learn\ModelModule\Api\Data\BookInterface
     */
    public function setAuthor($author);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ModelModule\Api\Data\BookExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Learn\ModelModule\Api\Data\BookExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ModelModule\Api\Data\BookExtensionInterface $extensionAttributes
    );
}
