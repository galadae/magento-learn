<?php


namespace Learn\ModelModule\Api\Data;

interface AuthorInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const AUTHOR_ID = 'id';
    const NAME = 'name';

    /**
     * Get author_id
     * @return string|null
     */
    public function getAuthorId();

    /**
     * Set author_id
     * @param string $authorId
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     */
    public function setAuthorId($authorId);

    /**
     * Get Name
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     * @param string $name
     * @return \Learn\ModelModule\Api\Data\AuthorInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Learn\ModelModule\Api\Data\AuthorExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Learn\ModelModule\Api\Data\AuthorExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Learn\ModelModule\Api\Data\AuthorExtensionInterface $extensionAttributes
    );
}
