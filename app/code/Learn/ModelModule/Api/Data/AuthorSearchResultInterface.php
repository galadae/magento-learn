<?php


namespace Learn\ModelModule\Api\Data;

interface AuthorSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Author list.
     * @return \Lear\ModelModule\Api\Data\AuthorInterface[]
     */
    public function getItems();

    /**
     * Set Name list.
     * @param \Lear\ModelModule\Api\Data\AuthorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
