<?php


namespace Learn\ModelModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BookRepositoryInterface
{

    /**
     * Save Book
     * @param \Learn\ModelModule\Api\Data\BookInterface $Book
     * @return \Learn\ModelModule\Api\Data\BookInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Learn\ModelModule\Api\Data\BookInterface $Book
    );

    /**
     * Retrieve Book
     * @param string $bookId
     * @return \Learn\ModelModule\Api\Data\BookInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bookId);

    /**
     * Retrieve Book matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Learn\ModelModule\Api\Data\BookSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Book
     * @param \Learn\ModelModule\Api\Data\BookInterface $Book
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Learn\ModelModule\Api\Data\BookInterface $Book
    );

    /**
     * Delete Book by ID
     * @param string $bookId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bookId);
}
