<?php

namespace Learn\ObserverModule\Console\Command;

use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ObserverAction extends Command
{

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    private $eventManager;

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        /** @var \Magento\Framework\DataObject $dataObject */
        $dataObject = $this->objectManager->create(\Magento\Framework\DataObject::class);
        $value = rand(1, 99);
        $dataObject->setData('value', $value);

        var_dump($dataObject->getData('value'));
        $this->eventManager->dispatch("TestEvent", ['dataObject' => $dataObject]);
        var_dump($dataObject->getData('value'));
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("learn:observer");
        $this->setDescription("SDesc");

        $this->objectManager = ObjectManager::getInstance();
        $this->eventManager = $this->objectManager->get(\Magento\Framework\Event\Manager::class);

        parent::configure();
    }
}
